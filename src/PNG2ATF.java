import com.google.gson.Gson;

import java.io.*;

public final class PNG2ATF
{

    private static String PNG2ATF;

    public static void main(String[] args)
    {
        //Config文件由第一个参数指定
        Config config = getJsonConfig(args[0]);
        PNG2ATF = config.png2atf;
        for (ConfigFileMeta fileMeta : config.fileList)
        {
            File inputFile = new File(fileMeta.input);
            if (inputFile.isDirectory())
            {
                for (File subFile : inputFile.listFiles())
                {
                    convertPNG2ATF(subFile, fileMeta.output, fileMeta.isDeleteOriginal);
                }
            }
            else
            {
                convertPNG2ATF(inputFile, fileMeta.output, fileMeta.isDeleteOriginal);
            }
        }
    }

    private static void convertPNG2ATF(File pngFile, String outPutFolder, Boolean isDeletePNG)
    {
        if (!pngFile.isDirectory() && getExtensionName(pngFile.getName()).equals("png"))
        {
            String fileName = getFileNameNoEx(pngFile.getName());
            String input = pngFile.getPath();
            String output;
            if (new File(outPutFolder).isDirectory())
            {
                output = outPutFolder + "/" + fileName + ".atf";
            }
            else
            {
                output = outPutFolder;
            }

            // ִ��ATF����
            Runtime run = Runtime.getRuntime();
            try
            {
                System.out.println("File: " + input);
                Process p = run.exec(PNG2ATF + " -i " + input + " -o " + output + " -e -r");
                p.waitFor();

                BufferedReader bf = new BufferedReader(new InputStreamReader(p.getInputStream()));
                String msg = null;

                BufferedReader err = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                String errMsg = null;

                do
                {
                    msg = bf.readLine();
                    if (msg != null && msg.length() > 1)
                    {
                        System.out.println(msg);
                    }
                } while (msg != null);

                Boolean isSucceed = true;
                do
                {
                    errMsg = err.readLine();
                    if (errMsg != null)
                    {
                        isSucceed = false;
                        System.out.println(errMsg);
                    }
                } while (errMsg != null);

                if (isSucceed)
                {
                    if (isDeletePNG)
                    {
                        pngFile.delete();
                    }
                }
                else
                {
                    new File(output).delete();
                }

            } catch (Exception e)
            {
                System.err.println(e.toString());
            }

            System.out.println("");
        }
    }

    /**
     * ��ȡ��׺��
     */
    public static String getExtensionName(String filename)
    {
        if ((filename != null) && (filename.length() > 0))
        {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length() - 1)))
            {
                return filename.substring(dot + 1);
            }
        }
        return filename;
    }

    /**
     * ȡ���ļ���
     */
    public static String getFileNameNoEx(String filename)
    {
        if ((filename != null) && (filename.length() > 0))
        {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length())))
            {
                return filename.substring(0, dot);
            }
        }
        return filename;
    }

    /**
     * ��ȡConfig�ļ�
     */
    private static Config getJsonConfig(String _configPath)
    {
        File f = new File(_configPath);
        Gson gson = new Gson();
        String configSTR = "";
        BufferedReader reader = null;
        try
        {
            reader = new BufferedReader(new FileReader(f));
            String tempString = null;
            while ((tempString = reader.readLine()) != null)
            {
                configSTR += tempString;
            }
            reader.close();
        } catch (IOException e)
        {
            System.err.println("Can not load Config.json file , check is this with jar at the same folder");
        }
        return gson.fromJson(configSTR, Config.class);
    }
}
